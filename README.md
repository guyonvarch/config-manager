Config Manager
==============

A configuration management library.

[http://hackage.haskell.org/package/config-manager](http://hackage.haskell.org/package/config-manager)

TODO
----

- Cumulative errors
- Add group support
- Show an error message when there is an import loop.
