module Data.ConfigManager.Types.Internal
  ( Config(..)
  , Expr(..)
  , Name
  , Value
  , Requirement(..)
  , Configured
  , convert
  ) where

import Data.Text (Text)
import Data.HashMap.Strict

-- | Configuration data.

data Config = Config
  { hashMap :: HashMap Name Value
  } deriving (Eq, Read, Show)

-- | An expression is either a binding or an import.

data Expr =
  Binding Name Value
  | Import Requirement FilePath
  deriving (Eq, Read, Show)

-- | A name is a text.

type Name = Text

-- | A value is a text.

type Value = Text

-- | A requirement is either required or optional.

data Requirement =
  Required
  | Optional
  deriving (Eq, Read, Show)

-- | This class represents types that can be converted /from/ a value /to/ a
-- destination type

class Configured a where
  convert :: Value -> Maybe a
