{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE IncoherentInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.ConfigManager.Instances
  () where

import qualified Data.Text as T
import Data.Time.Clock (DiffTime, NominalDiffTime)

import Text.Read (readMaybe)

import Data.ConfigManager.Types.Internal
import Data.ConfigManager.Parser.Duration (parseDuration)

instance Configured DiffTime where
  convert value = parseDuration value

instance Configured NominalDiffTime where
  convert value = realToFrac <$> parseDuration value

instance Read a => Configured a where
  convert = readMaybe . T.unpack
