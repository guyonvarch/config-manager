-- |
-- Module:      Data.ConfigManager.Config
-- License:     GPL-3
-- Maintainer:  Joris Guyonvarch <joris@guyonvarch.me>
-- Stability:   experimental
--
-- Configuration management types.

module Data.ConfigManager.Types
  ( Config(..)
  , Expr(..)
  , Name
  , Value
  , Requirement(..)
  , Configured
  , convert
  ) where

import Data.ConfigManager.Types.Internal
