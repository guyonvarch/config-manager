module Helper
  ( forceGetConfig
  , getConfig
  , eitherToMaybe
  ) where


import System.IO (hClose)
import System.IO.Temp (withSystemTempFile)
import System.Directory (removeFile)

import Data.Text (Text)
import qualified Data.Text.IO as T
import Data.Maybe (fromJust)

import Data.ConfigManager
import Data.ConfigManager.Types (Config)

forceGetConfig :: Text -> IO Config
forceGetConfig = (fmap fromJust) . getConfig

getConfig :: Text -> IO (Maybe Config)
getConfig input =
  withSystemTempFile "config-manager-test" (\filePath handle -> do
    hClose handle
    T.writeFile filePath input
    config <- readConfig filePath
    removeFile filePath
    return $ eitherToMaybe config
  )

eitherToMaybe :: Either a b -> Maybe b
eitherToMaybe (Left _) = Nothing
eitherToMaybe (Right x) = Just x
